# conversor_de_moedas

A new Flutter project.

## Ressalvas

Este é um projeto criado a partir de um curso da Udemy para fins de exercício e aprendizado. 
Ele foi realizado em conjunto com o instrutor, portanto você pode ter visto um projeto semelhante ou quase igual em outros locais.
Neste caso especifico a única diferença é a adição de Bitcoin nas moedas disponíveis para conversão.